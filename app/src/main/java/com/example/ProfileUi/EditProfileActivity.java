package com.example.ProfileUi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ProfileUI.R;

public class EditProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        String fullName = getIntent().getStringExtra(MainActivity.EXTRA_KEY_FULLNAME);
        final EditText editText = findViewById(R.id.et_editProfile);
        editText.setText(fullName);


        Button doneButton = findViewById(R.id.btn_editProfile_done);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullName = editText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra(MainActivity.EXTRA_KEY_FULLNAME,fullName);
                setResult(Activity.RESULT_OK,intent);
                finish();

            }
        });

    }
}
