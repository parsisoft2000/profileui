package com.example.ProfileUi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ProfileUI.R;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_KEY_FULLNAME = "fullName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Drawable drawable = getResources().getDrawable(R.drawable.profile_image);
        String text = "Hello World";

        //Save Information Button
        Button saveInformationBtn = findViewById(R.id.btn_main_saveInformation);

        saveInformationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "User Clicked On Saved Information Button", Toast.LENGTH_SHORT).show();
                //toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0,0);
                //toast.show();

            }
        });

        //Android Development CheckBox
        CheckBox androidDevelopmentCheckBox = findViewById(R.id.checkBox_main_androidDevelopment);

        androidDevelopmentCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "Android Skill CheckBox is Checked!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Android Skill CheckBox is not Checked!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        //Ui Design CheckBox
        CheckBox uiDesignCheckBox = findViewById(R.id.checkbox_main_uiDesign);

        uiDesignCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "Ui Desin CheckBox is Checked!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Ui Design CheckBox is not Checked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Deep Learning CheckBox
        CheckBox deepLearningCheckBox = findViewById(R.id.checkBox_main_deepLearning);

        deepLearningCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "Deep Learning CheckBox is Checked!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Deep Learning CheckBox is not Checked!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //BackEnd CheckBox
        CheckBox backEndCheckBox = findViewById(R.id.checkBox_main_backEnd);

        deepLearningCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(MainActivity.this, "BackEnd CheckBox is Checked!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "BackEnd CheckBox is not Checked!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //RadioGroup Instance
        RadioGroup radioGroup = findViewById(R.id.radioGroup_main);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton_main_alborz:
                        Toast.makeText(MainActivity.this, "Alborz RadioButton is Clicked!", Toast.LENGTH_SHORT).show();
                        /*Toast toast = Toast.makeText(MainActivity.this, "alborz selected", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.BOTTOM | Gravity.LEFT, 0, 0);
                        toast.show();*/

                        // Toast.makeText(MainActivity.this, "alborz selected", Toast.LENGTH_SHORT).show();

                        break;
                    case R.id.radioButton_main_gilan:
                        Toast.makeText(MainActivity.this, "Gilan RadioButton is Clicked!", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton_main_tehran:
                        Toast.makeText(MainActivity.this, "Tehran RadioButton is Clicked!", Toast.LENGTH_SHORT).show();
                    case R.id.radioButton_main_khouzestan:
                        Toast.makeText(MainActivity.this,"Khouzestan RadioButton is Clicked!",Toast.LENGTH_SHORT).show();
                        break;

                }


            }
        });
        //Implementation Explicit Intent Action On Edit Profile Button
        Button editProfileBtn = findViewById(R.id.btn_main_editProfile);
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditProfileActivity.class);
                TextView fullNameTv = findViewById(R.id.tv_main_fullName);
                intent.putExtra(EXTRA_KEY_FULLNAME, fullNameTv.getText());
                startActivityForResult(intent, 1001);
            }
        });

        //Implementation Implicit Intent Action On View Website Button
        Button viewWebsiteBtn = findViewById(R.id.btn_main_viewWebsite);
        viewWebsiteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://7learn.ac"));
                startActivity(intent);
            }
        });

        Log.v("MainActivity", "This is Verbose log", new NullPointerException());
        Log.println(Log.ASSERT, "Test", "This is a Assert log");


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001 && resultCode == Activity.RESULT_OK && data != null) {
            String fullName = data.getStringExtra(EXTRA_KEY_FULLNAME);
            TextView textview = findViewById(R.id.tv_main_fullName);
            textview.setText(fullName);

        }


    }


}

